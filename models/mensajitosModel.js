const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let mensajitosModel = new Schema({
    id_usuario:{
        type: String,
        required: [true, "El id de usuario es necesario"],
    },
    usuario_que_escribio:{
        type: String,
        required: [true, "El nombre es necesario"],
    },
    texto_mensaje: {
        type: String,
        required: [true, "El correo es necesario"],
    },

    fecha_creacion: {
        type: String,
        required: [true, "El password es necesario"],
    },

    sala: {
        type: String,
        // default: 'USER_ROLE',
        // enum: rolesValidos,
        required: [true, "La sala es necesaria"],
    },

});

module.exports = mongoose.model('mensajitosModel', mensajitosModel)