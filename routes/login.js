const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID);

const Usuario = require("../models/usuario");

const app = express();

app.post("/login", (req, res) => {
  
  let body = req.body;

  Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err,
      });
    }

    if (!usuarioDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: "(Usuario) o contraseña incorrectas",
        },
      });
    }
    
    if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
      return res.status(400).json({
        ok: false,
        err: {
          message: "Usuario o (contraseña) incorrectas",
        },
      });
    }

    let token = jwt.sign(
      {
        usuario: usuarioDB,
      },
      process.env.SEED,
      { expiresIn: process.env.CADUCIDAD_TOKEN }
    );

    console.log("QUEESESTO",token);

    res.json({
      ok: true,
      usuario: usuarioDB,
      token,
    });
  });
});

async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  });
  const payload = ticket.getPayload();

  // console.log(payload.name);
  // console.log(payload.email);
  // console.log(payload.picture);
  return payload;
}

app.post("/google", async (req, res) => {
  let token = req.body.tokenid;
  let googleUser = await verify(token).catch((e) => {
    return res.status(403).json({
      ok: false,
      err: e,
    });
  });

  // console.log("GOOGLEUSER",googleUser)

  //     cascascascsa
  //     const a = await promese
  //     console.log(a)//

  //   function asd() {
  //     return new Promise((resolve, reject) => {
  //       //logica
  //       let a = {}
  //       if (true) {
  //         resolve({data: asdasd});
  //       } else {
  //         reject(new Error("mensaje"));
  //       }
  //     });
  //   }
  // .then
  // .catch
  //   const aaea = await asd().catch(e => console.log(e.message))

  // console.log("@@@@@", googleUser);

  Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
    if (err) {
      console.log(err);
      return res.status(500).json({
        ok: false,
        err,
      });
    }

    if (usuarioDB) {
      if (usuarioDB.google === false) {
        return res.status(400).json({
          ok: false,
          err: {
            message: "Debe de usar su autenticación normal",
          },
        });
      } else {
        let token = jwt.sign(
          {
            usuario: usuarioDB,
          },
          process.env.SEED,
          { expiresIn: process.env.CADUCIDAD_TOKEN }
        );
        return res.json({
          ok: true,
          usuario: usuarioDB,
          token,
        });
      }
    } else {
      let usuario = new Usuario();

      usuario.nombre = googleUser.name;
      usuario.email = googleUser.email;
      usuario.img = "noimage.jpg";
      usuario.google = true;
      usuario.password = "123";

      usuario.save((err, usuarioDB) => {
        if (err) {
          console.log(err);
          return res.status(500).json({
            ok: false,
            err,
          });
        }
        let token = jwt.sign(
          {
            usuario: usuarioDB,
          },
          process.env.SEED,
          { expiresIn: process.env.CADUCIDAD_TOKEN }
        );

        return res.json({
          ok: true,
          usuario: usuarioDB,
          token,
        });
      });
    }
  });
});

module.exports = app;
