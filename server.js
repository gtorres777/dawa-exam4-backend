require("./config/config");
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");
const port = process.env.PORT || 3000;

//SOCKETS
// const http = require("http");
// const socketIO = require("socket.io");

const { Usuarios } = require("./sockets/classes/usuarios");
const { Mensajitos } = require("./sockets/classes/mensajitosClass");
const mensajitosModel = require("./models/mensajitosModel");

const { crearMensaje } = require("./sockets/utilidades/utilidades");


var server = app.listen(port);


var io = require('socket.io').listen(server);

const usuarios = new Usuarios();
let mis_mensajes = new Mensajitos()



app.use(cors());


io.on("connection", (client) => {
  console.log("Usuario conectado");

  client.on("entrarChat", async (data, callback) => {
    console.log("DATA",data)
    if (!data.nombre) {
      return callback({
        error: true,
        mensaje: "El nombre es necesario",
      });
    }else if(!data.sala){
        return callback({
            error: true,
            mensaje: "El nombre de sala es necesario",
        });
    }

    let { persona } = usuarios.agregarPersona(client.id, data.nombre, data.sala);
    let personas = usuarios.getPersonas(); 

    client.join(persona.sala)

    // @@EMM
    client.emit(
      'mensaje',
      crearMensaje(
        "Admin",
        `${persona.nombre}, bienvenido a la sala ${persona.sala}.`
      )
    );

    client
      .broadcast
      .to(persona.sala)
      .emit(
        'mensaje',
        crearMensaje("Admin",`${persona.nombre} se ha unido!`));
    

    const todos_los_mensajes = await mis_mensajes.getMensajitos(persona.sala)

    io.to(persona.sala).emit('listarPersonas', 
      {
        personas: await usuarios.getPersonasInSala(persona.sala),
        todos_los_mensajes: todos_los_mensajes, //chalius
        clientId: client.id
      });

    // client.broadcast.emit("listaPersonas", usuarios.getPersonas());

    callback(personas);
    // callback();
  });

  // chalius
  client.on("crearMensaje", async (data, callback) => {
    let persona = usuarios.getPersona(client.id);

    io.to(persona.sala).emit('mensaje', crearMensaje(persona.nombre, data));
    await mis_mensajes.agregarMensajito(persona.id, persona.nombre, data, "2020-07-16 19:37:04", persona.sala)
    callback();
  });

  // @EMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
  client.on("disconnect", async () => {
    let personaBorrada = usuarios.borrarPersona(client.id);
    if(personaBorrada){

        io.to(personaBorrada.sala)
          .emit('mensaje', crearMensaje("Admin", `${personaBorrada.nombre} salió`));

        io.to(personaBorrada.sala)
          .emit('listarPersonas', {personas: await usuarios.getPersonasInSala(personaBorrada.sala)});
    }


  });


});

//

app.use(express.static(path.join(__dirname, "dist")));
// app.use(express.static(path.join(__dirname, "public")))
app.use(bodyParser.urlencoded({ extender: false }));
app.use(bodyParser.json());

app.use(require("./routes/index"));

app.get("/salas", async (req, res) => {
 
    let _salas = await mensajitosModel.find({})
    
    let salas = _salas.map(i => i.sala)
    let other = []
    let c = salas.map(i => !other.includes(i) ? other.push(i) : -1)
    return res.json({
      salas: other
    })
})

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "dist", "index.html"));
});
// app.get("/",(req,res) => {
//     res.render("index.html")
// })
// app.use(require('./routes/usuario'));

mongoose.connect(
  process.env.URLDB,
  { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
  (err, res) => {
    if (err) throw err;
    console.log("Base de datos Online");
  }
);

// mongoose.connect('mongodb://localhost:27017/lab07DB', (err,res) =>{
//     if(err) throw err;

//     console.log('Base de datos Online')
// });

// app.listen(port, () => console.log(`Escuchando puerto ${port}`));

// app.listen(process.env.PORT, () =>
//   console.log(`Escuchando puerto ${process.env.PORT}`)
// );
