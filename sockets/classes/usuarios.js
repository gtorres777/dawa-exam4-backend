const Usuario = require("../../models/usuario");
const { collection } = require("../../models/usuario");

class Usuarios {
  constructor() {
    this.personas = [];
  }

  agregarPersona(id, nombre, sala) {
    nombre = nombre.trim();
    sala = sala.trim();

    let persona = { id, nombre, sala };

    this.personas.push(persona);

    return { persona };
  }

  getPersona(id) {
    let persona = this.personas.filter((persona) => persona.id === id)[0];

    return persona;
  }

  getPersonas() {
    return this.personas;
  }

  borrarPersona(id) {
    let personaBorrada = this.getPersona(id);

    this.personas = this.personas.filter((persona) => persona.id !== id);

    return personaBorrada;
  }

  // @EMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
  async getPersonasInSala(sala) {
    let personasensala = this.personas.filter(
      (persona) => persona.sala == sala
    );

    let rpta = await Promise.all(
      personasensala.map(async (user) => {
        const imgName = await this.getImage(user.nombre);
        return { ...user, img: imgName };
      })
    );
    return rpta;
  }

  // @EMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
  async getImage(nombre) {
    try {
      const user = await Usuario.findOne({ nombre: nombre });
      return user.img;
    } catch (err) {
      return "noimage.png";
    }
  }
}

module.exports = {
  Usuarios,
};
