const mensajitosModel = require("../../models/mensajitosModel");
// const { collection } = require("../../models/usuario");


class Mensajitos {
  // constructor() {
  //   this.mensajitos = [];
  // }

  async agregarMensajito(id_usuario,usuario_que_escribio, texto_mensaje, fecha_creacion,sala) {
    
    usuario_que_escribio = usuario_que_escribio.trim().toLowerCase();
    texto_mensaje = texto_mensaje.trim().toLowerCase();
    fecha_creacion = fecha_creacion.trim().toLowerCase();
    sala = sala.trim().toLowerCase();

    let mensajito =new mensajitosModel({
      id_usuario: id_usuario,
      usuario_que_escribio: usuario_que_escribio,
      texto_mensaje: texto_mensaje,
      fecha_creacion: fecha_creacion,
      sala: sala
    });
    try{
      const mensaje_guardado = await mensajito.save()
      console.log("mensaje guardado: ",mensaje_guardado)
    }catch(e){
      console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",e.message)
    }
    // this.mensajitos.push(mensajito);

    return {mensajito};
  }



  async getMensajitos(sala) {
    let mensajitos_de_sala = await mensajitosModel.find({sala: sala});
    return mensajitos_de_sala;
  }

  
}

module.exports = {
  Mensajitos,
};
